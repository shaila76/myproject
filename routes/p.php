<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
	return view('welcome');
});
	


route::get('/contact',function(){
	return view('pages.contact');
});
route::get('/about',function(){
	return view('about');

});
Route::get('/portfolio',function(){
		return view('pages.portfolio');
});
route::get('/service',function(){
	return view('service');
});
route::get('/team',function(){
	return view('team');
});
route::get('/register',function(){
	return view('register');
});
route::post('/store','MyController@store')->name('store');
route::get('/read','MyController@read')->name('read');
route::get('/mesg','myController@msg')->name('msg');
route::get('/view/{id}','MyController@views');
route::get('/edit/{id}','MyController@edit');
route::get('/delete/{id}','MyController@delete');
route::get('/edit/{id}','MyController@edit');
route::post('/update/{id}','MyController@update');
route::get('/img','MyController@image');
route::post('/imagestore','MyController@imagestore')->name('imgstore');
Route::get('/imgtable','MyController@imgtable')->name('imagetable');
Route::get('/singleimage/{id}','MyController@imgsingle')->name('singleimg');
Route::get('/deleteimage/{id}','MyController@imgdelete');
Route::get('/imageedit/{id}','MyController@imageedit');
Route::post('/updateimg/{id}','MyController@imageupdate');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/excel/export','MyController@excel')->name('excel.export');
