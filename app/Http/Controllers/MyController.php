<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
class MyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
     public function  index(){
        return view('about');
    }
    public function  store(Request $req){
        $validatedData = $req->validate([
        'name' => 'required|max:50|min:5',
        'email' => 'required|unique:users|max:50|min:5',
        'phone' =>'required|unique:users',
    ]);
        $data=array();
        $data['name']=$req->name;
        $data['email']=$req->email;
        $data['phone']=$req->phone;
        $newdata=DB::table('users')->insert($data);
        if ($newdata) {
            $notification = array(
    'message' => 'Post created successfully!',
    'alert-type' => 'success'
);

            return redirect('/read')->with($notification);
        }
        else{
            $notification = array(
    'message' => 'Post created failure!',
    'alert-type' => 'error'
);
            return redirect('/register')->with($notification);
        }
    }
    public function read(){
        $data1=DB::table('users')->get();
        return view('tables',compact('data1'));
    }
    public function msg(Request $request){
        $data2=array();
        $data2['name']=$request->name;
        $data2['email']=$request->email;
        $data2['phone']=$request->phone;
        $data2['message']=$request->message;
        $row=DB::table('contact')->insert($data2);
        if ($row) {
            return redirect('/');
        }
        else
            return redirect()->back();
    }
    public function views($id){
       $data= DB::table('users')->where('id',$id)->first();
        return view('singledata',compact('data'));

    }
    public function delete($id){
       $data3= DB::table('users')->where('id',$id)->delete();
       if ($data3) {

            $notification = array(
    'message' => 'deleted successfully!',
    'alert-type' => 'success'
);

            return redirect()->back()->with($notification);
       }
        

    }
    public function edit($id){
        $data=DB::table('users')->where('id',$id)->first();
        return view('dataedit',compact('data'));
    }
    public function  update(Request $req,$id){
        $validatedData = $req->validate([
        'name' => 'required|max:50|min:5',
        'email' => 'required|max:50|min:5',
        'phone' =>'required',
    ]);
        $data=array();
        $data['name']=$req->name;
        $data['email']=$req->email;
        $data['phone']=$req->phone;
        $newdata=DB::table('users')->where('id',$id)->update($data);
        if ($newdata) {
             $notification = array(
    'message' => 'Data updated successfully!',
    'alert-type' => 'success'
);

            return redirect('/read')->with($notification);
        }
        else{ $notification = array(
    'message' => 'Data update failure!',
    'alert-type' => 'error'
);
            
            return redirect()->route('/edit')->with($notification);
        }
    } 
    public function image(){
         $data1=DB::table('users')->get();
        return view('image',compact('data1'));
        
    }
    public function imagestore(Request $req){
        $validatedData = $req->validate([
       
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8048',
    ]);
        $data=array();
        $data['name']=$req->reg_id;
        $image=$req->file('image');
        $image_name=hexdec(uniqid());
        $ext=strtolower($image->getClientOriginalExtension());
        $new_name=$image_name.'.'.$ext;
        $path='public/image/';
        $image_url=$path.$new_name;
        $row=$image->move($path,$new_name);
        $data['images']=$image_url;
        $data1=DB::table('images')->insert($data);
        if ($data1) {
             $notification = array(
    'message' => 'Image stored successfully!',
    'alert-type' => 'success');
            return redirect()->route('imagetable')->with($notification); }
            else{
                $notification = array(
    'message' => 'image store failure!',
    'alert-type' => 'error'
);
                return redirect()->back()->with($notification);
            }
       

    }
    public function imgtable(){
        $data=DB::table('images')->join('users','images.name','users.id')->select('images.*','users.name')->get();
        return view('imagetable',compact('data'));
    }

    public function imgsingle($id){

        $data=DB::table('images')->join('users','images.name','users.id')->select('images.*','users.name')->where('images.id',$id)->first();
        return view('singleimage',compact('data'));
    }

    public function imgdelete($id){
        $image=DB::table('images')->where ('id',$id)->first();
        $img1=$image->images;
        unlink($img1);

        $data= DB::table('images')->where('id',$id)->delete();
        if ($data) {

            
             $notification = array(
    'message' => 'Image delete successfully!',
    'alert-type' => 'success');
            return redirect()->route('imagetable')->with($notification); 
        }
            else{
                $notification = array(
    'message' => 'Image delete failure!',
    'alert-type' => 'error'
);
                return redirect()->back()->with($notification);
            }
       
        }


        public function imageedit($id){
           $user= DB::table('users')->get();
           $image= DB::table('images')->where('id',$id)->first();
            return view('editimage',compact('user','image'));
            
        }

        public function imageupdate(Request $req,$id){

        $data=array();
        $data['name']=$req->reg_id;
        $image=$req->file('image');
        if ($image) {
          
        
        $image_name=hexdec(uniqid());
        $ext=strtolower($image->getClientOriginalExtension());
        $new_name=$image_name.'.'.$ext;
        $path='public/image/';
        $image_url=$path.$new_name;
        $row=$image->move($path,$new_name);
        $data['images']=$image_url;
    }
        $data1=DB::table('images')->where('id',$id)->update($data);
        if ($data1) {
            
            return redirect()->route('imagetable'); }
            else{
                
                return redirect()->back();
            }
       

    }


    public function excel(){
         return Excel::download(new UsersExport, 'users.xlsx');
    }
}
        

    
