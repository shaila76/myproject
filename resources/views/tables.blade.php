@extends('layout')
@section('service')
<section class="page-section" id="service">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">All students data</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
         <table id="myTable" class="table table-striped table-bordered" width="100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">create date</th>
    </tr>
  </thead>
  <tbody>
    <a href="{{route('excel.export)}}" class="btn btn-info" role="button">Download excel</a> 

    @foreach($data1 as $row)
    <tr>
      <th scope="row">{{$row->id}}</th>
      <td>{{$row->name}}</td>
      <td>{{$row->email}}</td>
      <td>{{$row->phone}}</td>
       <td>{{$row->created_at}}</td>
       <td><a href="{{url('/view/'.$row->id)}}" class="btn btn-info" role="button"><span class="glyphicon glyphicon-zoom-in"></span></a> 
          <a href="{{url('/edit/'.$row->id)}}" class="btn btn-info" role="button"><span class="glyphicon glyphicon-pencil"></span></a> 

          <a href="{{url('/delete/'.$row->id)}}" class="btn btn-danger" role="button"><span class="glyphicon glyphicon-minus"></span></a> </td>

    </tr>
    
    @endforeach
  </tbody>
</table>

          
        </div>
      </div>
    </div>
  </section>

@endsection
