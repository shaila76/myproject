@extends('layout')
@section('contact')
<section class="page-section" id="service">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Hello Mr./Miss {{$data->name}}</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
         <table class="table">
  
  <tbody>
    
    <tr>
      <th scope="row">{{$data->id}}</th>
      <td>{{$data->name}}</td>
      <td>{{$data->email}}</td>
      <td>{{$data->phone}}</td>
       <td>{{$data->created_at}}</td>
       
    </tr>
    <a href="{{url('/read')}}">GO BACK</a>>
    
  </tbody>
</table>

          
        </div>
      </div>
    </div>
  </section>

@endsection
