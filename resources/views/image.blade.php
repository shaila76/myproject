@extends('layout')
@section('contact')
<section class="page-section" id="service">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Image Upload Form</h2>
        
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
       @if ($errors->any())
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      
      <form action="{{route('imgstore')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="name">Name</label>
              <select class="form-control" id="name" name="reg_id" >
                @foreach($data1 as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endforeach
              </select>
            </div>
            
            <div class="form-group">
              <input class="form-control" id="email" type="file" name="image"  required="required">
              <p class="help-block text-danger"></p>
            </div>
            
          </div>
          
          <div class="clearfix"></div>
          <div class="col-lg-12 text-center">
            <div id="success"></div>
            <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Register </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</section>

@endsection
