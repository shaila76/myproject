@extends('layout')
@section('contact')
<section class="page-section" id="service">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Image Update Form</h2>
        
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
      
      
      <form action="{{url('/updateimg/'.$image->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="name">Name</label>
              <select class="form-control" id="name" name="reg_id" >
                @foreach($user as $row)
                <option value="{{$row->id}}" @if($row->id==$image->name)
                  {{"selected"}}
                   @endif>
                  
                
                {{$row->name}}</option>
                @endforeach
              </select>
            </div>
            
            <div class="form-group">
              <label>Profile picture</label>
              <input class="form-control" id="email" type="file" name="image"  required="required">
              <label>Profile picture</label>
              <img src="{{url($image->images)}}">
              <input class="form-control" id="email" type="hidden" name="oid_image"  value="{{$image->images}}">
              <p class="help-block text-danger"></p>
            </div>
            
          </div>
          
          <div class="clearfix"></div>
          <div class="col-lg-12 text-center">
            <div id="success"></div>
            <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">update </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</section>

@endsection
