@extends('layout')
@section('service')
<section class="page-section" id="service">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">All students data</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
         <table id="myTable" class="table table-striped table-bordered" width="100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Image</th>
      
    </tr>
  </thead>
  <tbody>
    @foreach($data as $row)
    <tr>
      <td scope="row" class="text-center">{{$row->id}}</td>
      <td class="text-center">{{$row->name}}</td>
      <td class="text-center"><img src="{{url($row->images)}}" height="100px" width="100px"></td>
      
       <td class="text-center"><a href="{{url('singleimage/'.$row->id)}}" class="btn btn-info" role="button">View</a> 
          <a href="{{url('/imageedit/'.$row->id)}}" class="btn btn-success" role="button">Edit</a> 

          <a href="{{url('/deleteimage/'.$row->id)}}" class="btn btn-danger" role="button">Delete</a> </td>

    </tr>
    
    @endforeach
  </tbody>
</table>

          
        </div>
      </div>
    </div>
  </section>

@endsection
